# -*- coding: utf-8 -*-
"""
.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html
"""

import logging
import os
import sys
import dropbox
import argparse
from flask import Flask, abort, Response
from flask import request, jsonify
import hmac
from hashlib import sha256
import json
import threading
import boto3
import io

app = Flask(__name__)

logger = logging.getLogger()  # Set logger for all submodules
global verbose


def config_logger(verbose=False, logdir="../log", logfile=None):
    """Configure general logger and severity.

    Args:
        verbose (boolean): true if DEBUG activated.
        logdir (str): Directory of log.
        logfile (str): File of the log.
    """
    global logger

    c_handler = logging.StreamHandler()
    if logfile is None:
        logfile = str(os.path.basename(sys.argv[0])).split(".")[0]
    f_handler = logging.FileHandler(logdir + "/" + logfile + ".log")

    # Create formatters and add it to handlers
    c_format = logging.Formatter(
        "%(asctime)s %(levelname)s %(filename)s %(lineno)d %(message)s"
    )
    f_format = logging.Formatter(
        "%(asctime)s %(levelname)s %(filename)s %(lineno)d %(message)s"
    )
    c_handler.setFormatter(c_format)
    f_handler.setFormatter(f_format)

    if verbose:
        logger.setLevel(logging.DEBUG)
        c_handler.setLevel(logging.DEBUG)
        f_handler.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        c_handler.setLevel(logging.INFO)
        f_handler.setLevel(logging.INFO)

    # Add handlers to the logger
    logger.addHandler(c_handler)
    logger.addHandler(f_handler)


def parse_args():
    """Parse the scripts args.

    Returns:
        parser (List): All arguments parsed.
    """
    parser = argparse.ArgumentParser(
        description="Transfer new files from Dropbox to S3"
    )
    optional = parser._action_groups.pop()
    # Manage required args
    # required = parser.add_argument_group('required arguments')
    # Manage optional args

    optional.add_argument(
        "--token",
        "-t",
        dest="dropbox_token",
        action="store",
        default=os.getenv("dropbox_token"),
        help="Token used to connect",
    )
    optional.add_argument(
        "--app-secret",
        "-a",
        dest="dropbox_app_secret",
        action="store",
        default=os.getenv("dropbox_app_secret"),
        help="Token used to connect",
    )
    optional.add_argument(
        "--logdir",
        "-L",
        dest="logdir",
        default=os.path.dirname(os.path.realpath(__file__)),
        action="store",
        help="Log directory to be used.",
    )
    optional.add_argument(
        "--verbose",
        "-v",
        dest="verbose",
        action="store_true",
        default=False,
        help="Activate verbose",
    )
    optional.add_argument(
        "--initialize",
        "-i",
        dest="initialize",
        action="store_true",
        default=False,
        help="If set, at first run it will upload all the current object in Dropbox to S3 bucket, default: false",
    )
    optional.add_argument(
        "--region",
        "-r",
        dest="aws_region",
        action="store",
        default="eu-west-1",
        help="AWS Account Region",
    )
    optional.add_argument(
        "--key-id",
        "-k",
        dest="aws_access_key_id",
        action="store",
        required=True,
        help="AWS Account access key ID",
    )
    optional.add_argument(
        "--key-secret",
        "-s",
        dest="aws_secret_access_key",
        action="store",
        required=True,
        help="AWS Account access key secret",
    )
    optional.add_argument(
        "--bucket-name",
        "-b",
        dest="aws_bucket_name",
        action="store",
        required=True,
        help="AWS Account access key secret",
    )
    optional.add_argument(
        "--listen-ip",
        "-l",
        dest="listen_ip",
        action="store",
        default="0.0.0.0",
        help="IP used by app to listen incoming webhook requests",
    )
    optional.add_argument(
        "--listen-port",
        "-p",
        dest="listen_port",
        action="store",
        default="5000",
        help="Port used by app to listen incoming webhook requests",
    )
    parser._action_groups.append(optional)
    return parser.parse_args()


@app.route("/")
def default_route():
    """Default route for webhook"""
    app.logger.info("Default route is called")
    status = 200
    return jsonify("Index of webhook"), status


@app.route("/webhook", methods=["GET"])
def challenge():
    """Respond to the webhook challenge (GET request) by echoing back the challenge parameter."""

    resp = Response(request.args.get("challenge"))
    resp.headers["Content-Type"] = "text/plain"
    resp.headers["X-Content-Type-Options"] = "nosniff"

    return resp


@app.route("/webhook", methods=["POST"])
def webhook():
    """Receive a list of changed user IDs from Dropbox and process each."""
    global dropbox_app_secret

    # Make sure this is a valid request from Dropbox
    try:
        signature = request.headers.get("X-Dropbox-Signature").encode("utf-8")
        if not hmac.compare_digest(
            signature,
            hmac.new(dropbox_app_secret.encode("utf-8"), request.data, sha256)
            .hexdigest()
            .encode("utf-8"),
        ):
            abort(
                jsonify(
                    "You don't have the permission to access the requested resource"
                ),
                403,
            )
        for account in json.loads(request.data)["list_folder"]["accounts"]:
            threading.Thread(target=process_user, args=(account,)).start()
    except Exception:
        logger.exception("Could not parse data")
        abort(
            jsonify("You don't have the permission to access the requested resource"),
            403,
        )
    return jsonify("Correctly processed request"), 200


def process_file(path, dbx, file_hash):
    """Processing of individual file

    Args:
        path (String): Path where to add the file in S3.
        dbx (dropbox.Dropbox): Dropbox object used for connection.
        file_hash (String): File hash, not used following https://github.com/boto/boto3/issues/845.
    """
    global aws
    meta, res = dbx.files_download(path)  # get file from dropbox
    s3 = boto3.client(
        "s3",
        aws_access_key_id=str(aws["access_key_id"]),
        aws_secret_access_key=str(aws["secret_access_key"]),
        region_name=str(aws["region_name"]),
    )  # Create S3 client
    logger.info("Uploading %s to %s" % (path.lstrip("/"), aws["bucket_name"]))
    s3.upload_fileobj(
        io.BytesIO(res.content), aws["bucket_name"], path.lstrip("/")
    )  # Uploading file


def process_user(account):
    """Call /files/list_folder for the given user ID and process any changes."""
    global args

    # cursor for the user (None the first time)
    try:
        with open("cursor.txt", "r") as reader:
            cursor = reader.read()
    except Exception:
        cursor = None

    dbx = dropbox.Dropbox(args.dropbox_token)
    has_more = True

    while has_more:
        if cursor is None:
            result = dbx.files_list_folder(path="", recursive=True)
        else:
            result = dbx.files_list_folder_continue(cursor)

        for entry in result.entries:
            # Ignore deleted files, folders, and non-markdown files
            if isinstance(entry, dropbox.files.DeletedMetadata) or isinstance(
                entry, dropbox.files.FolderMetadata
            ):
                continue
            if cursor is None and not args.initialize:
                logger.debug("Initialize not set, skipping processing")
                continue

            logger.info("New file with details: %s" % entry)
            threading.Thread(
                target=process_file, args=(entry.path_lower, dbx, entry.content_hash)
            ).start()

        # Update cursor (last update)
        cursor = result.cursor
        with open("cursor.txt", "w") as writer:
            writer.write(cursor)

        # Repeat only if there's more to do
        has_more = result.has_more


if __name__ == "__main__":
    global args
    args = parse_args()

    if not os.path.isdir(args.logdir):
        print("Error: Choosen log dir %s does not exist" % args.logdir)
        exit(1)

    if not args.dropbox_token:
        logger.error(
            "Mandatory arg dropbox token seems missing from args or env variable"
        )
        exit(1)

    if not args.dropbox_app_secret:
        logger.error(
            "Mandatory arg dropbox app secret seems missing from args or env variable"
        )
        exit(1)
    if not args.aws_bucket_name:
        logger.error(
            "Mandatory arg aws_bucket_name seems missing from args or env variable"
        )
        exit(1)

    global dropbox_app_secret
    dropbox_app_secret = args.dropbox_app_secret
    # S3
    global aws
    aws = {}
    aws["access_key_id"] = args.aws_access_key_id
    aws["secret_access_key"] = args.aws_secret_access_key
    aws["region_name"] = args.aws_region
    aws["bucket_name"] = args.aws_bucket_name

    config_logger(args.verbose, args.logdir)
    global verbose
    verbose = args.verbose

    gunicorn_logger = logging.getLogger("gunicorn.error")
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

    from waitress import serve

    serve(app, host=args.listen_ip, port=args.listen_port)
    # app.run(host=args.listen_ip)  # Debug Flask server
