# Dropbox2S3

## Purpose

Solve a need to transfer automatically new file in a Dropbox to a S3.
Based on Dropbox notification webhook, retrieve the file and then upload it to S3.

No integrity check are available via upload_fileobj S3, this is managed directly by boto3.
https://github.com/boto/boto3/issues/845

## Requirements

 * Python 3
 * Modules in requirements.txt
 * Access to S3 bucket with Write permission
 * Configure a application Webhook in Dropbox account targetting the application with at least 
   * account_info.read
   * files.metadata.read
   * files.content.read
 * Generate a token / app secret in the application to get files

## Dropbox Webhook configuration

In order to configure the webhook you can follow the link below:
https://www.dropbox.com/developers/reference/webhooks

Or just create it via the apps:

https://www.dropbox.com/developers/apps


## Installation

* You will need to install the library to use it

`pip install -r requirements.txt`


Please note that the usage of a virtualenv is recommended:
```
python3 -m venv venv
source venv/bin/activate
```

## Usage

The code should be run on a server with public IP to allow Dropbox registering the webhook.
It listen by default on all interface but this setting can be changed in args.

`python main.py -t "<dropbox_token>" -a "<dropbbox_app_secret>" -b <aws_bucket_name> -k <aws_key_id> -s <aws_key_secret>`

The Dropbox token is used to get the file, the app secret to ensure that the webhook call comes from Dropbox, AWS keys allow to
authenticate to the bucket and must have write permission on the bucket.

At first run, it will create a file named cursor.txt which will contains the last update hash from Dropbox.
In case you want to upload all the currents files from Dropbox to S3, please set option `-i`, else the upload will only
takes new files from current cursor.

## Development requirements

* Have a working Python 3 environment:
```
python3 -m venv venv
source venv/bin/activate
```
* Install the requirements
`pip install -r requirements-dev.txt`

## Automatical version publishing

Please note that this project use semantic-release for auto incrementation of
version and CHANGELOG.
If you want to build a version, you should use the Angular commit syntax.

More informations at:
https://github.com/semantic-release/semantic-release#commit-message-format
